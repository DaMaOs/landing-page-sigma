import { Component, Inject } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { SigninComponent } from './signin/signin.component';
import { SignupComponent } from './signup/signup.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public signInModel = {
    email: '',
    pass: ''
  };

  public signUpModel = {
    email: '',
    pass: '',
    name: '',
    lastName: '',
    opt: ''
  };

  constructor(private snackBar: MatSnackBar, public dialog: MatDialog) {
  }

  openSnackBar() {
    this.snackBar.open('Yo tampoco se que significa esa frase', '🙄', {
      duration: 3000,
      verticalPosition: 'bottom',
      horizontalPosition: 'left',
    });
  }

  openDialogSignIn(): void {
    const dialogRef = this.dialog.open(SigninComponent, {
      width: '400px',
      backdropClass: 'sgn-template',
      data: { email: this.signInModel.email}
    });
  }

  openDialogSignUp(): void {
    const dialogRef = this.dialog.open(SignupComponent, {
      width: '400px',
      backdropClass: 'sgn-template',
      data: { email: this.signUpModel.email, name: this.signUpModel.name, lastName: this.signUpModel.lastName}
    });
  }
}
